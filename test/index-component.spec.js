/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../index-component.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<index-component></index-component>");
    assert.strictEqual(_element.hello, "Hello World!");
  });
});
