import { html, css, LitElement } from "lit-element";
import "@element/dash-cards/dash-cards.js";
class IndexComponent extends LitElement {
  static get properties() {
    return {
      hello: { type: String }
    };
  }

  constructor() {
    super();
    this.hello = "Hello World!";
  }

  static get styles() {
    return css`
    .mainContainer{
      display:grid;
      grid-template-columns:repeat(2,1fr);
    }
    dash-cards{
      border:1px solid black;
    }
    `;
  }

  render() {
    return html`
      <div class="mainContainer">
        <dash-cards cardName="jjij" @card-clicked="${this.cardClicked}"></dash-cards>
        <dash-cards cardName="jjij" @card-clicked="${this.cardClicked}"></dash-cards>
        <dash-cards cardName="jjij" @card-clicked="${this.cardClicked}"></dash-cards>
        <dash-cards cardName="jjij" @card-clicked="${this.cardClicked}"></dash-cards>
      </div>
      
    `;
  }

  cardClicked(event){
    
  }

}

window.customElements.define("index-component", IndexComponent);
